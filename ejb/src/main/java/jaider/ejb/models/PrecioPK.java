package jaider.ejb.models;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the precios database table.
 * 
 */
@Embeddable
public class PrecioPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="id_lista_precio", insertable=false, updatable=false)
	private Integer idListaPrecio;

	@Column(name="id_producto", insertable=false, updatable=false)
	private Integer idProducto;

	public PrecioPK() {
	}
	
	public PrecioPK(Integer idListaPrecio, Integer idProducto) {
		super();
		this.idListaPrecio = idListaPrecio;
		this.idProducto = idProducto;
	}

	public Integer getIdListaPrecio() {
		return this.idListaPrecio;
	}
	public void setIdListaPrecio(Integer idListaPrecio) {
		this.idListaPrecio = idListaPrecio;
	}
	public Integer getIdProducto() {
		return this.idProducto;
	}
	public void setIdProducto(Integer idProducto) {
		this.idProducto = idProducto;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof PrecioPK)) {
			return false;
		}
		PrecioPK castOther = (PrecioPK)other;
		return 
			this.idListaPrecio.equals(castOther.idListaPrecio)
			&& this.idProducto.equals(castOther.idProducto);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.idListaPrecio.hashCode();
		hash = hash * prime + this.idProducto.hashCode();
		
		return hash;
	}

	@Override
	public String toString() {
		return "PrecioPK [idListaPrecio=" + idListaPrecio + ", idProducto=" + idProducto + "]";
	}
	
	
}