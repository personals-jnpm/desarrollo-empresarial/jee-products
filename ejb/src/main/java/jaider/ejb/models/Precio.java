package jaider.ejb.models;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the precios database table.
 * 
 */
@Entity
@Table(name="precios")
public class Precio implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private PrecioPK id;

	private BigDecimal valor;

	//bi-directional many-to-one association to ListaPrecio
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_lista_precio", insertable=false, updatable=false)
	private ListaPrecio listaPrecio;

	//bi-directional many-to-one association to Producto
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_producto", insertable=false, updatable=false)
	private Producto producto;

	public Precio() {
	}
	
	public Precio(BigDecimal valor, ListaPrecio listaPrecio) {
		super();
		this.valor = valor;
		this.listaPrecio = listaPrecio;
	}

	public PrecioPK getId() {
		return this.id;
	}

	public void setId(PrecioPK id) {
		this.id = id;
	}

	public BigDecimal getValor() {
		return this.valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public ListaPrecio getListaPrecio() {
		return this.listaPrecio;
	}

	public void setListaPrecio(ListaPrecio listaPrecio) {
		this.listaPrecio = listaPrecio;
	}

	public Producto getProducto() {
		return this.producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public void generarPK() {
		this.id = new PrecioPK();
		this.id.setIdProducto(this.producto.getIdProducto());
		this.id.setIdListaPrecio(this.listaPrecio.getIdListaPrecio());
	}
	
	@Override
	public String toString() {
		return "Precio [id=" + id + ", valor=" + valor + ", listaPrecio=" + listaPrecio + ", producto=" + producto
				+ "]";
	}
	
	

}