package jaider.ejb.models;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the lista_precios database table.
 * 
 */
@Entity
@Table(name="lista_precios")
public class ListaPrecio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_lista_precio")
	private Integer idListaPrecio;

	private Integer estado;

	@Column(name="nombre_lista_precio")
	private String nombreListaPrecio;

	//bi-directional many-to-one association to Precio
	@OneToMany(mappedBy="listaPrecio")
	private List<Precio> precios;

	//bi-directional many-to-one association to Usuario
	@OneToMany(mappedBy="listaPrecio")
	private List<Usuario> usuarios;

	public ListaPrecio() {
	}
	
	public ListaPrecio(Integer idListaPrecio) {
		super();
		this.idListaPrecio = idListaPrecio;
	}


	public Integer getIdListaPrecio() {
		return this.idListaPrecio;
	}

	public void setIdListaPrecio(Integer idListaPrecio) {
		this.idListaPrecio = idListaPrecio;
	}

	public Integer getEstado() {
		return this.estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public String getNombreListaPrecio() {
		return this.nombreListaPrecio;
	}

	public void setNombreListaPrecio(String nombreListaPrecio) {
		this.nombreListaPrecio = nombreListaPrecio;
	}

	public List<Precio> getPrecios() {
		return this.precios;
	}

	public void setPrecios(List<Precio> precios) {
		this.precios = precios;
	}

	public Precio addPrecio(Precio precio) {
		getPrecios().add(precio);
		precio.setListaPrecio(this);

		return precio;
	}

	public Precio removePrecio(Precio precio) {
		getPrecios().remove(precio);
		precio.setListaPrecio(null);

		return precio;
	}

	public List<Usuario> getUsuarios() {
		return this.usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	public Usuario addUsuario(Usuario usuario) {
		getUsuarios().add(usuario);
		usuario.setListaPrecio(this);

		return usuario;
	}

	public Usuario removeUsuario(Usuario usuario) {
		getUsuarios().remove(usuario);
		usuario.setListaPrecio(null);

		return usuario;
	}

	@Override
	public String toString() {
		return "ListaPrecio [idListaPrecio=" + idListaPrecio + ", estado=" + estado + ", nombreListaPrecio="
				+ nombreListaPrecio + ", precios=" + precios + ", usuarios=" + usuarios + "]";
	}

}