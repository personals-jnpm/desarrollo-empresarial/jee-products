package jaider.ejb.models;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the imagenes database table.
 * 
 */
@Entity
@Table(name="imagenes")
public class Imagene implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_imagen")
	private Integer idImagen;

	private String mime;

	@Column(name="nombre_inicial")
	private String nombreInicial;

	@Column(name="nombre_modificado")
	private String nombreModificado;

	private String tamanio;

	//bi-directional many-to-one association to Producto
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_producto")
	private Producto producto;

	public Imagene() {
	}

	public Integer getIdImagen() {
		return this.idImagen;
	}

	public void setIdImagen(Integer idImagen) {
		this.idImagen = idImagen;
	}

	public String getMime() {
		return this.mime;
	}

	public void setMime(String mime) {
		this.mime = mime;
	}

	public String getNombreInicial() {
		return this.nombreInicial;
	}

	public void setNombreInicial(String nombreInicial) {
		this.nombreInicial = nombreInicial;
	}

	public String getNombreModificado() {
		return this.nombreModificado;
	}

	public void setNombreModificado(String nombreModificado) {
		this.nombreModificado = nombreModificado;
	}

	public String getTamanio() {
		return this.tamanio;
	}

	public void setTamanio(String tamanio) {
		this.tamanio = tamanio;
	}

	public Producto getProducto() {
		return this.producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

}