package jaider.ejb.services;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import jaider.ejb.interfaces.FachadaJPA;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class ServiciosBasicos<T> {

	@EJB
	FachadaJPA<T> logica;

	public void crear(T tipoEntidad) throws Exception {
		logica.crear(tipoEntidad);
	}

	public boolean actualizar(T tipoEntidad) throws Exception {
		return logica.actualizar(tipoEntidad);
	}

	public void borrar(T tipoEntidad) throws Exception {
		logica.borrar(tipoEntidad);
	}

	public List<T> listarJPA(Class<T> tipoEntidad) throws Exception {
		return logica.listarJPA(tipoEntidad);
	}

	public List<T> listarNQUERY(Class<T> tipoEntidad) throws Exception {
		return logica.listarNQUERY(tipoEntidad);
	}

	public T buscar(Class<T> tipoEntidad, Integer identificador) throws Exception {
		return logica.buscar(tipoEntidad, identificador);
	}

	public List<T> listarJPA(Class<T> tipoEntidad, String propiedad) throws Exception {
		return logica.listarJPA(tipoEntidad, propiedad);
	}

	public List<T> listarNQUERY(Class<T> tipoEntidad, String namedQuery, String columna, Integer valor)
			throws Exception {
		return logica.listarNQUERY(tipoEntidad, namedQuery, columna, valor);
	}

	public List<T> buscarPorCampo(Class<T> tipoEntidad, String nombrePropiedad, String texto) throws Exception {
		return logica.buscarPorCampo(tipoEntidad, nombrePropiedad, texto);
	}
}
