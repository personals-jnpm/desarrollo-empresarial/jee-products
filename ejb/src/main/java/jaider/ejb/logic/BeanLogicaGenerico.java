package jaider.ejb.logic;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import jaider.ejb.interfaces.FachadaGenerica;
import jaider.ejb.utilidades.ImprimirObjeto;

@Stateless
@LocalBean
@TransactionManagement(TransactionManagementType.CONTAINER)
public class BeanLogicaGenerico implements FachadaGenerica {

	@PersistenceContext(unitName = "ejb_pu")
	EntityManager entidad;

	@Override
	public List<?> listadoConSubConsultas(String namedQuery) throws Exception {
		List<?> lista = new ArrayList<>();

		Query consulta = entidad.createNamedQuery(namedQuery);
		lista = consulta.getResultList();

		
		System.out.println("xxxx");
		for (Object obj : lista) {
			ImprimirObjeto.completo(obj);
		}
		
		return lista;
	}

}
