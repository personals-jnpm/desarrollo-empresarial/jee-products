package com.jaider.controladores;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import com.jaider.utilidades.MostrarMensaje;

import jaider.ejb.models.ListaPrecio;
import jaider.ejb.services.ServiciosBasicos;


@Named("beanListaPrecio")
@SessionScoped
public class ListaPreciosController implements Serializable {
	

	private static final long serialVersionUID = 1L;
	
	private ListaPrecio objListaPrecio;
	private List<ListaPrecio> ListaPrecios;
	
	@EJB
	ServiciosBasicos<ListaPrecio> servicioListaPrecio;

	public ListaPreciosController() {
		super();
		objListaPrecio = new ListaPrecio();
	}

	@PostConstruct
	public void init() {
		ListaPreciosJPA();
	}

	public ListaPrecio getObjListaPrecio() {
		return objListaPrecio;
	}

	public void setObjListaPrecio(ListaPrecio objListaPrecio) {
		this.objListaPrecio = objListaPrecio;
	}
	
	public List<ListaPrecio> getListaPrecios() {
		ListaPreciosJPA();
		return ListaPrecios;
	}

	public void setListadoListaPrecios(List<ListaPrecio> listadoListaPrecios) {
		this.ListaPrecios = listadoListaPrecios;
	}
	
	public String getEstadoNombre(Integer codigo) {
		String nombre;

		switch (codigo) {
		case 1:
			nombre = "Activo";
			break;
		default:
			nombre = "Inactivo";
			break;
		}
		return nombre;
	}

	public void btnCrear() {
		try {
			servicioListaPrecio.crear(objListaPrecio);
			objListaPrecio = new ListaPrecio(); 

			MostrarMensaje.pantalla("lista_precio_es_CO", "exito", "exitoCrear", "exitoCrearDetalle", "msgGrowl");
		} catch (Exception e) {
			MostrarMensaje.pantalla("lista_precio_es_CO", "fatal", "errorCrear", "errorCrearDetalle", "msgGrowl");
		}
	}
	
	public void btnActualizar() {
		try {
			servicioListaPrecio.actualizar(objListaPrecio);

			MostrarMensaje.pantalla("lista_precio_es_CO", "exito", "exitoEditar", "exitoEditarDetalle", "msgGrowl");
		} catch (Exception e) {
			MostrarMensaje.pantalla("lista_precio_es_CO", "fatal", "errorEditar", "errorEditarDetalle", "msgGrowl");
		}
	}
	
	public void btnBorrar() {
		try {
			servicioListaPrecio.borrar(objListaPrecio);
			objListaPrecio = new ListaPrecio();

			MostrarMensaje.pantalla("lista_precio_es_CO", "exito", "exitoBorrar", "exitoBorrarDetalle", "msgGrowl");
		} catch (Exception e) {
			MostrarMensaje.pantalla("lista_precio_es_CO", "fatal", "errorBorrar", "errorBorrarDetalle", "msgGrowl");
		}
	}

	public void ListaPreciosJPA() {
		try {
			ListaPrecios = servicioListaPrecio.listarJPA(ListaPrecio.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String seleccionarListaPrecioActualizar(ListaPrecio temp) {
		objListaPrecio = temp;
		return "editar.faces";
	}
	
	public void seleccionarListaPrecioBorrar(ListaPrecio temp) {
		objListaPrecio = temp;
	}
	
}
