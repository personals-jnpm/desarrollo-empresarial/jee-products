package com.jaider.controladores;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import com.jaider.utilidades.Formulario;
import com.jaider.utilidades.MostrarMensaje;
import com.jaider.utilidades.Propiedades;

import jaider.ejb.models.ListaPrecio;
import jaider.ejb.models.Precio;
import jaider.ejb.models.Producto;
import jaider.ejb.services.ServiciosBasicos;

@Named("beanPrecio")
@SessionScoped
public class PrecioController implements Serializable {

	private static final long serialVersionUID = 1L;

	private Precio objPrecio;
	private List<Precio> listadoPrecios;

	// Variables de combo
	private Integer idComboListaPrecio;
	private Integer idComboProducto;
	private List<SelectItem> listaPreciosCargada;

	@Inject
	ProductoController productController;

	@EJB
	ServiciosBasicos<Precio> servicioPrecio;

	@EJB
	ServiciosBasicos<Producto> servicioProducto;

	@EJB
	ServiciosBasicos<ListaPrecio> servicioListaPrecio;

	public PrecioController() throws Exception {
		super();
		initObject();
	}

	public void initObject() throws Exception {
		objPrecio = new Precio();
		objPrecio.setListaPrecio(new ListaPrecio());
		idComboListaPrecio = null;
		idComboProducto = null;
		getComboListaPreciosVacia();
	}

	@PostConstruct
	public void init() throws Exception {
		getComboListaPreciosVacia();
	}

	public Precio getobjPrecio() {
		return objPrecio;
	}

	public void setobjPrecio(Precio objPrecio) {
		this.objPrecio = objPrecio;
	}

	public List<Precio> getListadoPrecios() {
		listadoPreciosJPA();
		return listadoPrecios;
	}

	public void setListadoPrecios(List<Precio> listadoPrecios) {
		this.listadoPrecios = listadoPrecios;
	}

	public Precio getObjPrecio() {
		return objPrecio;
	}

	public void setObjPrecio(Precio objPrecio) {
		this.objPrecio = objPrecio;
	}

	public Integer getIdComboListaPrecio() {
		return idComboListaPrecio;
	}

	public void setIdComboListaPrecio(Integer idComboListaPrecio) {
		this.idComboListaPrecio = idComboListaPrecio;
	}

	public Integer getIdComboProducto() {
		return idComboProducto;
	}

	public void setIdComboProducto(Integer idComboProducto) {
		this.idComboProducto = idComboProducto;
	}

	public List<SelectItem> getListaPreciosCargada() {
		return listaPreciosCargada;
	}

	public void setListaPreciosCargada(List<SelectItem> listaPreciosCargada) {
		this.listaPreciosCargada = listaPreciosCargada;
	}

	public void btnCrearPrecio() {
		try {
			Producto producto = new Producto();
			producto = servicioProducto.buscar(Producto.class, idComboProducto);

			ListaPrecio listaPrecio = new ListaPrecio();
			listaPrecio = servicioListaPrecio.buscar(ListaPrecio.class, idComboListaPrecio);

			objPrecio.setProducto(producto);
			objPrecio.setListaPrecio(listaPrecio);
			objPrecio.generarPK();

			servicioPrecio.crear(objPrecio);

			initObject();

			MostrarMensaje.pantalla("precio_es_CO", "exito", "exitoCrear", "exitoCrearDetalle", "msgGrowl");

		} catch (Exception e) {
			MostrarMensaje.pantalla("precio_es_CO", "fatal", "errorCrear", "errorCrearDetalle", "msgGrowl");
		}
	}

	public List<SelectItem> getComboProducto() throws Exception {
		String mensajeVacio = Propiedades.obtener("/precio_es_CO", "selectProductoEmpty");
		List<Producto> listaProductos = servicioProducto.listarJPA(Producto.class);
		return Formulario.armarCombo(listaProductos, mensajeVacio, "getIdProducto", "getNombreProducto");
	}

	public void getComboListaPrecios() throws Exception {
		String mensajeVacio = Propiedades.obtener("/precio_es_CO", "selectListaPrecioEmpty");
		List<ListaPrecio> listaPrecios = servicioListaPrecio.listarNQUERY(ListaPrecio.class, "DISPONIBLE_POR_PRODUCTO", "codPro", idComboProducto);
		listaPreciosCargada = Formulario.armarCombo(listaPrecios, mensajeVacio, "getIdListaPrecio", "getNombreListaPrecio");
	}
	
	public void getComboListaPreciosVacia() throws Exception {
		String mensajeVacio = Propiedades.obtener("/precio_es_CO", "selectListaPrecioEmpty");
		listaPreciosCargada = Formulario.armarCombo(null, mensajeVacio, "getIdListaPrecio", "getNombreListaPrecio");
	}

	public void btnCrear() {
		try {
			objPrecio.setProducto(productController.getObjProducto());
			objPrecio.generarPK();
			
			servicioPrecio.actualizar(objPrecio);
			initObject();

			MostrarMensaje.pantalla("precio_es_CO", "exito", "exitoCrear", "exitoCrearDetalle", "msgGrowl");
		} catch (Exception e) {
			MostrarMensaje.pantalla("precio_es_CO", "fatal", "errorCrear", "errorCrearDetalle", "msgGrowl");
		}
	}

	public void btnBorrar() {
		try {
			servicioPrecio.borrar(objPrecio);

			objPrecio = new Precio();

			MostrarMensaje.pantalla("precio_es_CO", "exito", "exitoBorrar", "exitoBorrarDetalle", "msgGrowl");
		} catch (Exception e) {
			MostrarMensaje.pantalla("precio_es_CO", "fatal", "errorBorrar", "errorBorrarDetalle", "msgGrowl");
		}
	}

	public void listadoPreciosJPA() {
		try {
			listadoPrecios = servicioPrecio.listarJPA(Precio.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<Precio> listadoPreciosNQUERY() {
		try {
			return servicioPrecio.listarNQUERY(Precio.class, "PRODUCTOS", "codigoProducto",
					productController.getObjProducto().getIdProducto());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String seleccionarPrecioActualizar(Precio temp) {
		objPrecio = temp;
		return "editar.faces";
	}

	public void seleccionarPrecioBorrar(Precio temp) {
		objPrecio = temp;
	}
	
	

}
