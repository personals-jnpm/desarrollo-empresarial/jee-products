package com.jaider.controladores;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.model.SelectItem;
import javax.inject.Named;

import com.jaider.utilidades.MostrarMensaje;

import jaider.ejb.models.Categoria;
import jaider.ejb.models.Producto;
import jaider.ejb.services.ServiciosBasicos;

@Named("beanProducto")
@SessionScoped
public class ProductoController implements Serializable {

	private static final long serialVersionUID = 1L;

	private Producto objProducto;
	private List<Producto> listadoProductos;
	
	@EJB
	ServiciosBasicos<Producto> servicioProducto;
	
	@EJB
	ServiciosBasicos<Categoria> servicioCategoria;

	public ProductoController() {
		super();
		resetearObjeto();
	}

	@PostConstruct
	public void init() {
		listadoProductosNQUERY();
	}

	public Producto getObjProducto() {
		return objProducto;
	}

	public void setObjProducto(Producto objProducto) {
		this.objProducto = objProducto;
	}

	public List<Producto> getListadoProductos() {
		listadoProductosNQUERY();
		return listadoProductos;
	}

	public void setListadoProductos(List<Producto> listadoProductos) {
		this.listadoProductos = listadoProductos;
	}

	public void btnCrear() {
		try {
			
			servicioProducto.crear(objProducto);
			
			objProducto = new Producto();
			objProducto.setCategoria(new Categoria());
			
			MostrarMensaje.pantalla("producto_es_CO", "exito", "exitoCrear", "exitoCrearDetalle", "msgGrowl");
		} catch (Exception e) {
			MostrarMensaje.pantalla("producto_es_CO", "fatal", "errorCrear", "errorCrearDetalle", "msgGrowl");
		}
	}
	
	public void btnActualizar() {
		try {
			servicioProducto.actualizar(objProducto);
			
			
			MostrarMensaje.pantalla("producto_es_CO", "exito", "exitoEditar", "exitoEditarDetalle", "msgGrowl");
		} catch (Exception e) {
			MostrarMensaje.pantalla("producto_es_CO", "fatal", "errorEditar", "errorEditarDetalle", "msgGrowl");
		}
	}
	
	public void btnBorrar() {
		try {
			servicioProducto.borrar(objProducto);
			
			objProducto = new Producto();
			objProducto.setCategoria(new Categoria());
			
			MostrarMensaje.pantalla("producto_es_CO", "exito", "exitoBorrar", "exitoBorrarDetalle", "msgGrowl");
		} catch (Exception e) {
			MostrarMensaje.pantalla("producto_es_CO", "fatal", "errorBorrar", "errorBorrarDetalle", "msgGrowl");
		}
	}
	
	// Otra forma de obtener el combo
	public List<SelectItem> getComboCategoria(){
		try {
			List<SelectItem> combo = new ArrayList<>();
			List<Categoria> listaCategorias = servicioCategoria.listarJPA(Categoria.class);
			
			for (Categoria categoria: listaCategorias) {
				SelectItem  item = new SelectItem(categoria.getIdCategoria(), categoria.getNombreCategoria());
				combo.add(item);
			}
			return combo;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}

	public void listadoProductosJPA() {
		try {
			listadoProductos = servicioProducto.listarJPA(Producto.class, "categoria");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void listadoProductosNQUERY() {
		try {
			listadoProductos = servicioProducto.listarNQUERY(Producto.class, "OBTENER-PRODUCTOS", "", 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String seleccionarProductoActualizar(Producto temp) {
		objProducto = temp;
		return "editar.faces";
	}

	public void seleccionarProductoBorrar(Producto temp) {
		objProducto = temp;
	}
	
	public void resetearObjeto() {
		objProducto = new Producto();
		objProducto.setCategoria(new Categoria());
	}

}
