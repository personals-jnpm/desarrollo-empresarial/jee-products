package com.jaider.controladores;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import com.jaider.utilidades.MostrarMensaje;

import jaider.ejb.models.Categoria;
import jaider.ejb.services.ServiciosBasicos;
import jaider.ejb.services.ServiciosGenericos;

@Named("beanCategoria")
@SessionScoped
public class CategoriaController implements Serializable {

	private static final long serialVersionUID = 1L;

	private Categoria objCategoria;
	private List<Categoria> listadoCategorias;
	private List<?> listaCategoriaGenerica;

	@EJB
	ServiciosBasicos<Categoria> servicioCategoria;

	@EJB
	ServiciosGenericos servicioGenericoCategoria;

	public CategoriaController() {
		super();
		initCategoria();
	}

	@PostConstruct
	public void init() {
		listadoCategoriasNQUERY();
	}

	public Categoria getObjCategoria() {
		return objCategoria;
	}

	public void setObjCategoria(Categoria objCategoria) {
		this.objCategoria = objCategoria;
	}

	public List<Categoria> getListadoCategorias() {
		listadoCategoriasNQUERY();
		return listadoCategorias;
	}

	public void setListadoCategorias(List<Categoria> listadoCategorias) {
		this.listadoCategorias = listadoCategorias;
	}

	public List<?> getListaCategoriaGenerica() {
		return listaCategoriaGenerica;
	}

	public void setListaCategoriaGenerica(List<?> listaCategoriaGenerica) {
		this.listaCategoriaGenerica = listaCategoriaGenerica;
	}

	public void initCategoria() {
		objCategoria = new Categoria();
	}

	public String getEstadoNombre(Integer codigo) {
		String nombre;

		switch (codigo) {
		case 1:
			nombre = "Activo";
			break;
		default:
			nombre = "Inactivo";
			break;
		}
		return nombre;
	}

	public void btnCrear() {
		try {
			List<Categoria> categorias = new ArrayList<>();

			categorias = servicioCategoria.buscarPorCampo(Categoria.class, "nombreCategoria",
					objCategoria.getNombreCategoria());

			if (categorias.size() == 0) {
				servicioCategoria.crear(objCategoria);
				initCategoria();
				MostrarMensaje.pantalla("categoria_es_CO", "exito", "exitoCrear", "exitoCrearDetalle", "msgGrowl");
			} else {
				MostrarMensaje.pantalla("categoria_es_CO", "advertencia", "advertenciaCrear", "advertenciaCrearDetalle", "msgGrowl");
				for (Categoria categoria : categorias) {
					System.out.println(categoria.getNombreCategoria());
				}
			}

		} catch (Exception e) {
			MostrarMensaje.pantalla("categoria_es_CO", "fatal", "errorCrear", "errorCrearDetalle", "msgGrowl");
		}
	}

	public void btnActualizar() {
		try {
			servicioCategoria.actualizar(objCategoria);

			MostrarMensaje.pantalla("categoria_es_CO", "exito", "exitoEditar", "exitoEditarDetalle", "msgGrowl");
		} catch (Exception e) {
			MostrarMensaje.pantalla("categoria_es_CO", "fatal", "errorEditar", "errorEditarDetalle", "msgGrowl");
		}
	}

	public void btnBorrar() {
		try {
			servicioCategoria.borrar(objCategoria);
			initCategoria();

			MostrarMensaje.pantalla("categoria_es_CO", "exito", "exitoBorrar", "exitoBorrarDetalle", "msgGrowl");
		} catch (Exception e) {
			MostrarMensaje.pantalla("categoria_es_CO", "fatal", "errorBorrar", "errorBorrarDetalle", "msgGrowl");
		}
	}

	public void listadoCategoriasJPA() {
		try {
			listadoCategorias = servicioCategoria.listarJPA(Categoria.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void listadoCategoriasNQUERY() {
		try {
			listadoCategorias = servicioCategoria.listarNQUERY(Categoria.class, "POR-ESTADOS-E-HIJOS", "codigoEstado",
					1);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<?> listadoCategoriasGenerico() throws Exception {
		listaCategoriaGenerica = servicioGenericoCategoria.listadoConSubConsultas("Categoria.CANTIDAD-PRODUCTOS");
		return listaCategoriaGenerica;
	}

	public String seleccionarCategoriaActualizar(Categoria temp) {
		objCategoria = temp;
		return "editar.faces";
	}

	public void seleccionarCategoriaBorrar(Categoria temp) {
		objCategoria = temp;
	}

}
