
package com.jaider.controladores;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import com.jaider.utilidades.HashPassword;
import com.jaider.utilidades.MostrarMensaje;

import jaider.ejb.models.Categoria;
import jaider.ejb.models.ListaPrecio;
import jaider.ejb.models.Usuario;
import jaider.ejb.services.ServiciosBasicos;

@Named("beanUsuario")
@SessionScoped
public class UsuarioController implements Serializable {

	private static final long serialVersionUID = 1L;

	private Usuario objUsuario;
	private List<Usuario> listadoUsuarios;
	private String claveActual;

	@EJB
	ServiciosBasicos<Usuario> servicioUsuario;

	@EJB
	ServiciosBasicos<Categoria> servicioCategoria;

	public UsuarioController() {
		super();
		reset();
	}

	@PostConstruct
	public void init() {
		listadoUsuariosJPA();
	}

	public void reset() {
		objUsuario = new Usuario();
		objUsuario.setListaPrecio(new ListaPrecio());
	}

	public Usuario getObjUsuario() {
		return objUsuario;
	}

	public void setObjUsuario(Usuario objUsuario) {
		this.objUsuario = objUsuario;
	}

	public List<Usuario> getListadoUsuarios() {
		listadoUsuariosJPA();
		return listadoUsuarios;
	}

	public void setListadoUsuarios(List<Usuario> listadoUsuarios) {
		this.listadoUsuarios = listadoUsuarios;
	}

	public String getClaveActual() {
		return claveActual;
	}

	public void setClaveActual(String claveActual) {
		this.claveActual = claveActual;
	}

	public String getGeneroNombre(Integer codigo) {
		String nombre;

		switch (codigo) {
		case 1:
			nombre = "Masculino";
			break;
		case 2:
			nombre = "Femenino";
			break;
		default:
			nombre = "Otro";
			break;
		}
		return nombre;
	}

	public void btnCrear() {
		try {

			objUsuario.setClave(HashPassword.hashPBKDF2(objUsuario.getClave()));
			servicioUsuario.crear(objUsuario);

			reset();
			MostrarMensaje.pantalla("usuario_es_CO", "exito", "exitoCrear", "exitoCrearDetalle", "msgGrowl");
		} catch (Exception e) {
			MostrarMensaje.pantalla("usuario_es_CO", "fatal", "errorCrear", "errorCrearDetalle", "msgGrowl");
		}
	}

	public void btnActualizar() {
		try {
			servicioUsuario.actualizar(objUsuario);

			MostrarMensaje.pantalla("usuario_es_CO", "exito", "exitoEditar", "exitoEditarDetalle", "msgGrowl");
		} catch (Exception e) {
			MostrarMensaje.pantalla("usuario_es_CO", "fatal", "errorEditar", "errorEditarDetalle", "msgGrowl");
		}
	}

	public void btnBorrar() {
		try {
			servicioUsuario.borrar(objUsuario);

			objUsuario = new Usuario();

			MostrarMensaje.pantalla("usuario_es_CO", "exito", "exitoBorrar", "exitoBorrarDetalle", "msgGrowl");
		} catch (Exception e) {
			MostrarMensaje.pantalla("usuario_es_CO", "fatal", "errorBorrar", "errorBorrarDetalle", "msgGrowl");
		}
	}

	public void listadoUsuariosJPA() {
		try {
			listadoUsuarios = servicioUsuario.listarJPA(Usuario.class, "listaPrecio");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void listadoUsuariosNQUERY() {
		try {
			// listadoUsuarios = servicioUsuario.listarNQUERY(Usuario.class,
			// "OBTENER-USUARIOS", "", 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String seleccionarUsuarioActualizar(Usuario temp) {
		objUsuario = temp;
		return "editar.faces";
	}

	public void seleccionarUsuarioBorrar(Usuario temp) {
		objUsuario = temp;
	}

	public void btnActualizarClave() {
		try {
			System.out.println("-----"+HashPassword.hashPBKDF2(claveActual));
			System.out.println("-----"+objUsuario.getClave());
			if (objUsuario.getClave().equals(HashPassword.hashPBKDF2(claveActual))) {
				servicioUsuario.actualizar(objUsuario);
				MostrarMensaje.pantalla("usuario_es_CO", "exito", "exitoEditar", "exitoEditarClaveDetalle", "msgGrowl");
			}else {
				MostrarMensaje.pantalla("usuario_es_CO", "advertencia", "errorEditar", "errorEditarClaveDifDetalle",
						"msgGrowl");
			}
			
		} catch (Exception e) {
			MostrarMensaje.pantalla("usuario_es_CO", "fatal", "errorEditar", "errorEditarClaveDetalle", "msgGrowl");
			System.out.println("---------- "+e.getMessage());
		}
	}

	public String seleccionarUsuarioActualizarClave(Usuario temp) {
		objUsuario = temp;
		return "editar-clave.faces";
	}

}
