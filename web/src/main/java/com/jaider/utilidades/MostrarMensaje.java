package com.jaider.utilidades;

import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;

public class MostrarMensaje {

	private static void construir(Severity tipo, String titulo, String detalle, String identificador) {
		if (identificador.equals("")) {
			identificador = null;
		}

		FacesMessage mensaje = new FacesMessage(tipo, titulo, detalle);
		FacesContext.getCurrentInstance().addMessage(identificador, mensaje);
	}

	private static void error(String titulo, String detalle, String identificador) {
		MostrarMensaje.construir(FacesMessage.SEVERITY_ERROR, titulo, detalle, identificador);
	}

	private static void exito(String titulo, String detalle, String identificador) {
		MostrarMensaje.construir(FacesMessage.SEVERITY_INFO, titulo, detalle, identificador);
	}

	private static void advertencia(String titulo, String detalle, String identificador) {
		MostrarMensaje.construir(FacesMessage.SEVERITY_WARN, titulo, detalle, identificador);
	}

	private static void fatal(String titulo, String detalle, String identificador) {
		MostrarMensaje.construir(FacesMessage.SEVERITY_FATAL, titulo, detalle, identificador);
	}

	public static void pantalla(String archivoPropiedades, String tipo, String titulo, String detalle,
			String identificador) {
		String tituloFinal= ResourceBundle.getBundle(archivoPropiedades).getString(titulo);
		String detalleFinal= ResourceBundle.getBundle(archivoPropiedades).getString(detalle);
	
		switch (tipo) {
		case "exito":
			MostrarMensaje.exito(tituloFinal, detalleFinal, identificador);
			break;
		case "error":
			MostrarMensaje.error(tituloFinal, detalleFinal, identificador);
			break;
		case "advertencia":
			MostrarMensaje.advertencia(tituloFinal, detalleFinal, identificador);
			break;
		default:
			MostrarMensaje.fatal(tituloFinal, detalleFinal, identificador);
			break;
		}
	}

}
