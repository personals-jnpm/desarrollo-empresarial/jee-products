package com.jaider.utilidades;

import java.util.ResourceBundle;

import javax.faces.context.FacesContext;

public class MensajesProperties {
	public static String getMessagePropertyProduct(String key) {
		FacesContext context = FacesContext.getCurrentInstance();
		ResourceBundle property = ResourceBundle.getBundle("producto",
				context.getViewRoot().getLocale());
		return property.getString(key);
	}
	
	public static String getMessagePropertyCategory(String key) {
		FacesContext context = FacesContext.getCurrentInstance();
		ResourceBundle property = ResourceBundle.getBundle("categoria",
				context.getViewRoot().getLocale());
		return property.getString(key);
	}
	
	public static String getMessagePropertyListPrice(String key) {
		FacesContext context = FacesContext.getCurrentInstance();
		ResourceBundle property = ResourceBundle.getBundle("lista_precio",
				context.getViewRoot().getLocale());
		return property.getString(key);
	}
}
