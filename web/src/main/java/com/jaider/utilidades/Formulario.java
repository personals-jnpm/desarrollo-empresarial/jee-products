package com.jaider.utilidades;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

public class Formulario {

	private static List<SelectItem> comboFinal;

	public static List<SelectItem> getComboFinal() {
		return comboFinal;
	}

	public static void setComboFinal(List<SelectItem> comboFinal) {
		Formulario.comboFinal = comboFinal;
	}

	private static void opcionDefecto(String titulo) {
		if (titulo != null && !titulo.isEmpty()) {
			comboFinal.add(new SelectItem(null, titulo));
		}
	}

	private static void opciones(List<?> lista, String code, String text) throws NoSuchMethodException,
			SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		if (lista != null) {
			for (Object obj : lista) {
				Class<?> classObj = obj.getClass();
				Method metodoCod = classObj.getDeclaredMethod(code);
				Method metodoVal = classObj.getDeclaredMethod(text);

				Integer codigo = (Integer) metodoCod.invoke(obj);
				String mostrar = (String) metodoVal.invoke(obj);

				comboFinal.add(new SelectItem(codigo, mostrar));
			}
		}
	}

	public static List<SelectItem> armarCombo(List<?> lista, String titulo, String metodoCodigo, String metodoTexto)
			throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException,
			InvocationTargetException {
		comboFinal = new ArrayList<>();
		opcionDefecto(titulo);
		opciones(lista, metodoCodigo, metodoTexto);

		return comboFinal;
	}
}
